/* SPDX-License-Identifier: MIT */

const path = require('path');
const fs = require('fs');

const folderPath = path.join(__dirname, 'products');
const deviceDirectories = fs.readdirSync(folderPath);

/*  Data format (including file content).:
    {
      DEVNAM1: {file1:"file1_data", file2:"file2_data, ..., fileN:"fileN_data"},
      DEVNAM2: {file1:"file1_data", file2:"file2_data, ..., fileN:"fileN_data"},
      ...
    }

    Summary of Data format (no file content, just names)
    {
      DEVNAM1: [file1, file2, ...],
      DEVNAM2: [file1, file2, ...],
      ...
    }
*/

summaryDataAll = () => {
    var summaryData = {};
    deviceDirectories.forEach( (prodnam) => {
        summaryData[prodnam] = [];

        var devDir = path.join(folderPath, prodnam);
        var dirContents = fs.readdirSync(devDir);
        dirContents.forEach( (filename) => {
    	    if (filename != ".keepdir") {
                summaryData[prodnam].push(filename);
    	    }
        });

    });
    return summaryData;
}
summaryDataDevice = (prodnam) => {
    var summaryData = {};
    const devDir = path.join(folderPath, prodnam);
    if (fs.existsSync(devDir)) {
	summaryData[prodnam] = [];
	var dirContents = fs.readdirSync(devDir);
	//console.log(`${prodnam}:  ${dirContents}`);
	dirContents.forEach( (filename) => {
	    if (filename != ".keepdir") {
		summaryData[prodnam].push(filename);
	    }
	});
    }
    return summaryData;
}

layoutDataDevice = (prodnam) => {
    var fileData = {};
    const devDir = path.join(folderPath, prodnam);
    if (fs.existsSync(devDir)) {
	var dirContents = fs.readdirSync(devDir);
	dirContents.forEach( (filename) => {
	    if (filename != ".keepdir") {
		var filePath = path.join(devDir, filename);
		var buffer = fs.readFileSync(filePath);
		fileData[filename] = buffer;
	    }
	});
    }
    //console.log(`fileData: ${JSON.stringify(fileData)}`);

    return fileData;
}



module.exports = {

    testing () {
	return `XKEYS-LAYOUTS ${__dirname}`;
    },

    /*  summary(...productNames)
    *
    *   Return a summary of available files for the given productNames
    */
    summary (...productNames) {
	if (productNames.length == 0) {
	    return summaryDataAll();
	} else {
	    var allData = [];
	    productNames.forEach( (prodnam) => {
		allData.push(summaryDataDevice(prodnam));
	    });
	    return Object.assign({}, ...allData);
	}
    },

    /*  fetch(...productNames)
    *
    *   Return available file data for the given productNames
    */
    fetch (...productNames) {
        var allData = {};
        if (productNames.length > 0) {
            productNames.forEach( (prodnam) => {
                allData[prodnam] = layoutDataDevice(prodnam);
            });
        }
        return allData;
    }

}


